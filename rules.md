# NOMIC V RULESET
---
## 101 (IMMUTABLE)

All players must always abide by all the rules then in effect, in the form in which they are then in effect. The rules in the Initial Set are in effect whenever a game begins. The Initial Set consists of Rules 101-116 (immutable) and 201-216 (mutable).

## 102 (IMMUTABLE)

Initially rules in the 100's are immutable and rules in the 200's are mutable. Rules subsequently enacted or transmuted (that is, changed from immutable to mutable or vice versa) may be immutable or mutable regardless of their numbers, and rules in the Initial Set may be transmuted regardless of their numbers.

## 103 (IMMUTABLE)

A rule-change is any of the following: (1) the enactment, repeal, or amendment of a mutable rule; (2) the enactment, repeal, or amendment of an amendment of a mutable rule; or (3) the transmutation of an immutable rule into a mutable rule or vice versa.

## 104 (IMMUTABLE)

All proposed rule-changes must be written down. If they are adopted, they shall guide play in the form in which they were voted on.

## 105 (IMMUTABLE)

All rule-changes put to a vote in the proper way shall be adopted if and only if they receive the required number of votes.

## 106 (IMMUTABLE)

No rule-change may take effect earlier than the moment of the completion of the vote that adopted it, even if its wording explicitly states otherwise. No rule-change may have retroactive application.

## 107 (IMMUTABLE)

Each proposed rule-change put to a vote shall be given a number for reference. The numbers shall begin with 301, and each proposed rule-change put to a vote in the proper way shall receive the next successive integer, whether or not the proposal is adopted.

When a rule-change is adopted in the proper way, the rule it enacts receives said rule-change’s number.

## 108 (IMMUTABLE)

Rule-changes that transmute immutable rules into mutable rules may be adopted if and only if the vote is unanimous among the eligible voters. Transmutation shall not be implied, but must be stated explicitly in a proposal to take effect.

## 109 (IMMUTABLE)

In a conflict between a mutable and an immutable rule, the immutable rule takes precedence and the mutable rule shall be entirely void. For the purposes of this rule a proposal to transmute an immutable rule does not "conflict" with that immutable rule.

If two or more mutable rules conflict with one another, or if two or more immutable rules conflict with one another, then the rule with the lowest ordinal number takes precedence.

If at least one of the rules in conflict explicitly says of itself that it defers to another rule (or type of rule) or takes precedence over another rule (or type of rule), then such provisions shall supersede the numerical method for determining precedence.

If two or more rules claim to take precedence over one another or to defer to one another, then the numerical method again governs.

## 110 (IMMUTABLE)

A player always has the option to forfeit the game rather than continue to play or incur a game penalty. No penalty worse than losing, in the judgment of the player to incur it, may be imposed.

## 111 (IMMUTABLE)

The adoption of rule-changes must never become completely impermissible.

## 112 (IMMUTABLE)

Rules can change their own text or repeal themselves. Rules cannot make any other changes to the rules unless the explicit authority for such a change is already present.

## 113 (IMMUTABLE)

Votes must be unambiguous. A vote that would be ambiguous is not considered to be a vote. Additional rules may be made to clarify what constitutes ambiguity. 

## 114 (IMMUTABLE)

Server moderators regulate the server and game. Their responsibilities include the creation of text and voice channels, the designation of which messages and actions go in which channels, the editing or deleting of messages that are inappropriate for the channel that they are in, and keeping track of the state of the game.

This game adheres to the Discord Terms of Service and the Discord Community Guidelines. Moderators are empowered to remove content that violates the terms and guidelines as well as sanction violating persons, up to and including banning.

A person is a server moderator if and only if they have the "Moderator" role.

## 115 (IMMUTABLE)

All actions, including voting, endorsing, and rule-change proposals, must occur in the channel or channels that are designated for them. Any action that does not occur in said action’s designated channel or channels is not considered a valid action.

## 116 (IMMUTABLE)

Whatever is not prohibited or regulated by a rule is permitted and unregulated, with the sole exception of changing the rules, which is permitted only when a rule or set of rules explicitly or implicitly permits it.


## 201

The Proposal Queue is a queue that proposed rule-changes, or proposals, go on. Players may have up to one proposal in the Proposal Queue. Active players may create a proposal to go on the Proposal Queue at any time; if a player that already has a proposal on the Proposal Queue creates another one, the first proposal is removed. Players may also remove their proposals from the Proposal Queue without replacement. Proposals may not be edited.

## 202

Active players may endorse any number of proposals in the Proposal Queue, including their own. Proposals in the Proposal Queue are sorted first in descending order of number of active endorsing players, then in ascending order of proposal creation time.

## 203

One turn consists of taking a proposal off the front of the Proposal Queue, and having a vote on whether or not to adopt it. The voting period ends when the turn ends.
If the proposal queue is empty at the start of the turn, then no vote will be held. The duration of the turn will be unaffected.

The length of a turn defaults to 24 hours.

## 204

A rule-change is adopted if and only if it receives a simple majority of votes in favor of its passing. Every active player is an eligible voter.

Other rules may introduce additional conditions necessary for a proposed rule-change to be adopted, notwithstanding the above paragraph.

## 205

An adopted rule-change takes full effect at the moment of the start of the turn following the turn that it was adopted on.

## 206

All players start the game as active. An active player becomes inactive (*i.e.* loses their active status) if said active player has publicly declared that they are now inactive. Other rules may also grant and revoke activity status.

A player that has become inactive via public declaration may become active again by attempting to cast a vote or endorse a proposal (the vote or endorsement will be counted as though the player had cast or granted it while active) or by publicly declaring that they are active.

## 207

Players that are required by the rules to perform an action have a total of 48 hours to perform said action. A player that does not perform said action within the allotted 48 hours becomes inactive until either they do perform said action or said action becomes impossible to perform.

## 208

Each player always has exactly one vote.

## 209

Players may change their votes by voting again. Players may withdraw their votes with the phrase “withdraw”. Players may not edit or delete their votes (*i.e.* alter the voting record). A vote that is edited loses its vote status.

Players may grant and withdraw their endorsements freely and at any time.

## 210

Case will not be considered when determining the ambiguity of a vote.
The only phrase to be considered an unambiguous vote and in favor shall be "pog".
The only phrase to be considered an unambiguous vote and not in favor shall be "sus".

## 211

If a section of a rule relies on a bot to be implemented to the point where its manual implementation would be infeasible, and there is no such bot, then said rule’s section is void until said bot is functional.

## 212

All players start with 0 points. The winner is the first player to achieve 100 (positive) points.

## 213

Points are separated into distinct levels.  
These levels are at every number in the Fibonacci sequence.(1,2,3,5,8...etc)  
If no player has surpassed a level in 3 months, then this game of nomic will end, and the persons with the highest number of points are declared the winners.

Every time a player surpasses a level, they are allowed to receive one reward.  
Rewards may become available at different levels, have stipulations, or be one time offers.  
Future rules may define new rewards.  
Players may offer things or services as rewards.  
Every time that player's reward is delivered, that player gets 1 point.  
Rule defined rewards do not grant points.  
Players may add or remove a reward they offer by stating so in actions.

The following are an initial set of rewards:

* The player may have their role color changed (6th level and above, once per reward).
* The player may request the removal or addition of a custom emoji with the same approval process as rule 301.

## 214

If players disagree about the legality of a move or the interpretation or application of a rule, then an eligible player is to be the Judge and decide the question. Said player is to be selected uniformly at random. Disagreement for the purposes of this rule may be created by the insistence of any player. This process is called invoking Judgment.

A player is eligible to be selected as Judge if and only if all of the following are true:

* The player is active
* The player has voted on the most recent proposal put to vote, or the player has voted on the second most recent proposal put to vote, or no more than two proposals have been put to vote since the start of the game
* The player has not proposed a rule-change currently being voted on
* The player has not yet been Judge this turn
* The player has not explicitly opted out of the pool of eligible Judges
* The player has not implicitly opted out of the pool of eligible Judges by declining to issue any Judgment during a turn that they are Judge

When Judgment is invoked for the first time in a turn, the current turn is extended by 24 hours unless the scheduled end of the turn was already more than 24 hours after Judgment’s invocation. If the turn is extended in this manner, and there are still more than 24 hours left in the turn, a majority of active players may consent to revoke the extension.

A Judge's ruling may be overruled only by either a unanimous vote of the other active players within 24 hours of the ruling or a majority vote of server moderators within 24 hours of the ruling. A Judge's ruling can be overruled regardless of the state of the game.

If a Judge's ruling is overruled on the same turn that it was made, then another eligible player is selected uniformly at random to become the new Judge for the turn, and so on. If a Judge's ruling is overruled on a different turn than the one it was made on, then a player is again selected uniformly at random to become a Judge for that turn, but the length of the turn is not extended unless Judgment is explicitly called during the turn.

The server moderators may only overrule a Judge's ruling if the Judgment egregiously conflicts with the rules and would cause lasting damage to the game. A majority vote of the other active players within 24 hours of the overruling may nullify it, in which case the original ruling stands, all rulings issued by Judges between the overruling and the nullification of the overruling are nullified, and the server moderators are barred from overruling the Judge for the remainder of the turn.

If an overruling was nullified on the same turn that it was made, then the original Judge resumes their position as Judge.

Unless a Judge is overruled, one Judge settles all questions arising from the game until the next turn is begun, including questions as to his or her own legitimacy and jurisdiction as Judge.

New Judges are not bound by the decisions of old Judges. New Judges may, however, settle only those questions on which the players currently disagree and that affect the completion of the turn in which Judgment was invoked. All decisions by Judges shall be in accordance with all the rules then in effect; but when the rules are silent, inconsistent, or unclear on the point at issue, then the Judge shall consider game-custom and the spirit of the game before applying other standards.

Players that have opted out of the pool of eligible Judges may opt in again by explicitly stating so in #actions.

## 215

If the rules are changed so that further play is impossible, or if the legality of a move cannot be determined with finality, or if by the Judge's best reasoning, not overruled, a move appears equally legal and illegal, then the proposer of the proposal up for vote is the winner. If there is no proposal up for vote, then the proposer of the proposal at the front of the Proposal Queue at the time the game has become unplayable is the winner. If the Proposal Queue is empty, then there is no winner.

This rule takes precedence over every other rule determining the winner.

## 216

Persons wishing to become players may join by requesting to do so publicly. Persons may not act as multiple players at once. Persons that are found to have done this are banned from the game.

## 301

Each player can request 1 and only 1 emoji to be added to the discord. Emoji requests are made in actions and are reviewed by all 4 moderators to determine if they are appropriate or break the discord TOS and community guidelines. Players must have the emoji image posted as an image in their request.

## 302

Discord bots may or may not interact with the game in a meaningful way. Interacting with the game in a meaningful way means that one or more of the bot’s functions is described in the rules. This can be directly through explicit mention of the bot being intended to perform a function or indirectly if anything described in the rules is fulfilled by a bot in practice. Discord bots which interact with the game in a meaningful way must be open source.

## 306

If a player invokes Judgement in accordance with Rule 214, the player that invoked Judgement must wait one full turn before being able to invoke Judgement again.

## 307

No proposal may distinguish between players based on how they endorse or vote for that proposal.

## 308

The set of emojis allowed to be used in this server is the set of emojis default in all discord chats and the set of emojis specific to this server

## 314

the two voice chats will be named "town-square" and "tavern"

## 316

Races are teams that players belong to exactly one of. A player not belonging to a Race must choose a Race to join; if they do not by the end of the turn, a Race will be randomly chosen for them. A race is eligible to be chosen, both voluntarily or randomly, unless a rule explicitly states otherwise. Rules may define additional Races beyond the ones present in this rule.

Players may attempt to convert any specific player of a different race to the player’s own Race. This is done by stating so in #actions. Races may have additional restrictions for when a conversion attempt can be made, additional effects that take place upon a conversion attempt, or the ability to attempt to convert multiple players with a single conversion attempt.

If a player is targeted by a conversion attempt, that player immediately converts to the race that made said attempt. A different member of the race that said player was on at the time that they were targeted by the conversion attempt may counter said conversion attempt, in which case the converted player converts back to the countering player’s race. A conversion attempt can only be countered on the same turn that it was made. A single counter does not counter the entirety of a multi-player conversion attempt.

All players in a Race can collectively either attempt one conversion or counter one conversion per turn. If a single player belonging to a given Race performs either of those actions, then subsequently neither action can be performed by any players of said Race until the next turn.

Players start out with 0 Superiority Points (SP). SP is a distinct entity from points. A Race is considered to have a number of SP equal to the sum of the SP counts of all of its players.

Races compete with each other in the Race Race. The first Race to have 400 SP wins the Race Race. When this occurs, all players belonging to the winning Race gain 25 points, after which this rule is repealed.

There are several ways that players can gain SP:

* Post an image of their Race in #race-race; this image can not be the same image as any previous image used by any player under this clause (1 SP, max 1 image per turn)
* Defeat a player of a different Race in a game defined by one singular rule (2 SP, max 1 game per turn)
* Convert a player from a different team to your team (5 SP, SP gained is lost if said player’s conversion is countered)

The following are Races:

* Werewolf
* Merfolk
* Goblin
* Bird Person
* Toaster

The Lunar Calendar can be in one of four phases: New Moon, First Quarter, Full Moon, and Second Quarter. At the start of every turn, the Lunar Calendar is set to the lunar phase for Mexico City according to timeanddate.com (https://www.timeanddate.com/moon/phases/mexico/mexico-city) with all times rounded to the the latest preceding midnight according to the CDT timezone. The Werewolf Race can only attempt a conversion if the Lunar Calendar is set to Full Moon.

The Weather can be in one of three states: Sunny, Cloudy, or Rainy; at the start of every turn, the Weather is set to one of these states with respective probabilities of 3/5, 1/5, and 1/5. The Merfolk Race can only attempt a conversion if the Weather is set to Rainy.

A conversion attempt from the Goblin Race has a 5/6 chance of failing.

A week is defined as a period of time starting and ending on 12:00 AM CDT Sunday. The Bird Person Race can only attempt one conversion every week. When attempting a conversion, there is a 25% chance that two random players are chosen for conversion instead of the targeted player.

@Nomitron has a 1/333 chance of posting the message “Let’s Get This Bread!” in #actions at the start of every hour. The Toaster Race can only attempt a conversion if the above event has occurred earlier in the turn. A conversion attempt by the Toaster race can target up to four players, but no three targeted players may be of the same race.

## 325

The most recent proposal in #voting shall be pinned. This shall be unpinned when it is no longer the most recent proposal. All other messages in #voting shall not be pinned.

## 331

Any player may request the removal of any post or proposal that contains any personal identifying information

## 340

Proposal to add a time system to the game along with multiverse structure, referred to as the Great Wheel. The Great Wheel is currently 40 days away from from its first turning.

## 354

@Crorem

## 356

-6+12[a6/9(6*g)\@89]

## 373

All players who have had a proposal pass to become either a rule or an edit win and the game ends. This supercedes all other rules on the topic of winning